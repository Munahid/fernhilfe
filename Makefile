# Makefile for hilfe (call for help) and helfer (run viewer and wait for the call)

CC = g++
LFLAGS = -static-libgcc -static-libstdc++ -static -Wl,--subsystem,windows
CFLAGS = -std=c++2a -O2 -Wall
LIBS = -lkernel32 -luser32 -lgdi32 -lpathcch

SRCS := $(wildcard src/*.cpp)
PRGS := $(SRCS:src/%.cpp=bin/%.exe)

bin/%.exe : src/%.cpp
	@echo "Building $@..."
	$(CC) $(CFLAGS) $< $(LFLAGS) $(LIBS) -o $@
	strip $@

all: $(PRGS)

.PHONY: clean
clean:
	@echo "Cleaning up..."
	rm bin/*.exe
