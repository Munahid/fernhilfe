# Fernhilfe project (needs a VNC viewer and server, e.g. Ultra-VNC)

## About

The Fernhilfe (german for "remote assistance") project contains two executables, "hilfe" and "helfer" (german words meaning help and helper). With "hilfe" a user can call for help and choose a helping person to provide assistance. The "helfer" executable simply runs a VNC viewer which locally waits for an incoming connection.

This tool works similar like the well-known [Ultra-VNC Single Click](https://uvnc.com/products/uvnc-sc.html) solution. Its an executable which a user may run to allow an administrator access to the user's desktop without installing an always accessible server (as a service) on the client's side. Internally it starts a VNC server while the administrator uses a client (viewer) which waits for an incoming connection. It is able to execute every product you like, not only UltraVNC.

The files are pre-configured to use an (even portable) installation of a current Ultra-VNC. This application must also be configured in advance (setting a password or cryptography plugins, call uvnc_settings.exe to create the needed UltraVNC.ini). Firewall rules may also have to be adapted or supplemented.

## Building

The sources need a MinGW/gcc and a Windows operating system to compile. We suggest installing an MSYS2 environment. First install (and update) [MSYS2](https://www.msys2.org/) and you're able to add everything you need. If you want to compile the project yourself, you need (additionally) git to check out and MinGW's GNU C/C++ to compile the project.

```
git clone git@gitlab.com:Munahid/fernhilfe.git
cd fernhilfe
git pull origin main
```

You have to adapt user names and paths to executables in the configuration file and the icons. The shown logo is a simple file in BMP format.

## License

The content of this project is subject to the [MIT license](LICENSE).
For more details please look at the LICENSE file in the root folder of the repository.

Please note that Ultra-VNC is licensed under the GNU GPL 2+. The Fernhilfe project does not represent a combined software with Ultra-VNC. The point is indeed the common use of both projects, but this project is an independent starter software for any such applications. It is possible to use other VNC software. For example the last RealVNC (run its viewer with e.g. "-ProtocolVersion=3.3 -listen" if you have a mixed setup) or TightVNC.

## Links

* https://uvnc.com/downloads/ultravnc.html
* https://github.com/ultravnc/UltraVNC/tree/main/vncviewer

## Known Bugs

* Calling "hilfe" without running the viewer on the helper's side fails and the server on the caller's side waits forever. Kill it with the Task-Manager.

## Planned Features

* Use the Gilab CI/CCD pipeline to build this project.
* Texts for the users what to do.
* Checks if everything we need is accessable.
* Check, if helper is online, else gray out the button.
* Homepage
