// Startet einen UltraVNC-Server nach Wahl des Nutzers.
// @license MIT <https://spdx.org/licenses/MIT>

#ifndef UNICODE
#define UNICODE
#endif 
#ifndef _UNICODE
#define _UNICODE
#endif 

#define WIN32_LEAN_AND_MEAN     // Exclude rarely-used stuff from Windows headers

#include <windows.h>
#include <strsafe.h>
#include <pathcch.h>

#include <algorithm>      // std::remove_if()
#include <cctype>         // isspace()
#include <iostream>
#include <fstream>
#include <map>
#include <string>

// Retrieve the system error message for the last-error code.

void ErrorExit(LPTSTR lpszFunction)
{
  LPVOID lpMsgBuf;
  LPVOID lpDisplayBuf;
  DWORD dw = GetLastError();

  FormatMessage(
    FORMAT_MESSAGE_ALLOCATE_BUFFER |
    FORMAT_MESSAGE_FROM_SYSTEM |
    FORMAT_MESSAGE_IGNORE_INSERTS,
    NULL,
    dw,
    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
    (LPTSTR) &lpMsgBuf,
    0, NULL);

  // Display the error message and exit the process.

  lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
      (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
  StringCchPrintf((LPTSTR)lpDisplayBuf,
    LocalSize(lpDisplayBuf) / sizeof(TCHAR),
    TEXT("%s failed with error %d: %s"),
    lpszFunction, dw, lpMsgBuf);
  MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

  LocalFree(lpMsgBuf);
  LocalFree(lpDisplayBuf);
  ExitProcess(dw);
}

/*
std::wstring utf8ToUtf16(const std::string &utf8Str)
{
  std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> conv;
  return conv.from_bytes(utf8Str);
}

std::string utf16ToUtf8(const std::wstring& utf16Str)
{
  std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> conv;
  return conv.to_bytes(utf16Str);
}
*/

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

HWND     mainwin    = nullptr;
HBITMAP  logoBitmap = NULL;
HWND     txt        = nullptr;
HWND     b1, b2, b3, b4; // Buttons to call someone
WCHAR    filename[MAX_PATH];
HBRUSH   hBrush = CreateSolidBrush(RGB(255,255,255));

std::map<std::string, std::string> config = {};

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow)
{
  UNREFERENCED_PARAMETER(hPrevInstance);
  UNREFERENCED_PARAMETER(pCmdLine);
  UNREFERENCED_PARAMETER(nCmdShow);

  // Gets the directory of the current binary and reads config.txt.

  GetModuleFileNameW(NULL, filename, MAX_PATH);
  //PathRemoveFileSpec(filename);
  PathCchRemoveFileSpec(filename, MAX_PATH);
  //GetCurrentDirectory(MAX_PATH, filename);
  wcscat_s(filename, MAX_PATH, L"\\config.txt");

  {
    std::ifstream configFile(filename);
    if (configFile.is_open()) {
      std::string line;
      std::string name;
      std::string value;
      std::size_t pos;

      while (getline(configFile, line)) {
        if (line[0] == '#')                      // Ignore comment lines.
          continue;

        pos = line.find("#");                    // Find comment starting somewhere in the line.
        if (pos != std::string::npos) {          // Found a "#".
          line = line.substr(0, pos);            // Save the left part of the line.
        }

        if (line.empty())                        // Ignore empty lines.
          continue;

        pos = line.find(" = ");                  // Find a delimiter. We use always " = ", not "=".
        if (pos == std::string::npos) {          // There wasn't a " = " within the line.
          std::cout << "No equal delimeter in line: " << line << std::endl;
          continue;
        }

        name = line.substr(0, pos);
        value = line.substr(pos + 3);

        // Remove all whitespace: "C" locale: ' ', \f, \n, \r, \t, \v; not \b.
        //name.erase(std::remove_if(name.begin(), name.end(), isspace), name.end());
        //std::erase_if(name, [](char c) { return std::isspace(c) || c == ','; });
        std::erase_if(name, ::isspace);

        // Ignore empty lines.
        if (name.empty()) {
          std::cout << "Left part of name-value-tupel was empty." << std::endl;
          continue;
        }

        config.insert({name, value});   // config[name] = value;
        std::cout << name << " " << value << std::endl;
      }
    }
    else {
      std::wcerr << L"Couldn't open config file for reading."  << std::endl;
    }
  }

  const wchar_t CLASS_NAME[]  = L"Fernhilfe Class";

  WNDCLASSEX wc = { };

  wc.lpfnWndProc   = WindowProc;
  wc.hInstance     = hInstance;
  wc.lpszClassName = CLASS_NAME;
  wc.style         = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
  wc.cbSize        = sizeof(WNDCLASSEX);
  wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
  wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);
  wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
  wc.lpszMenuName  = NULL;                     // No menu.
  wc.cbClsExtra    = 0;                        // No extra bytes after the window class
  wc.cbWndExtra    = 0;                        // structure or the window instance.
  wc.hbrBackground = (HBRUSH)COLOR_BACKGROUND; // Windows's default color as window background.

  if (!RegisterClassEx(&wc))
    return 0;

  std::string str = config["window.title"];
  std::wstring ws = std::wstring(str.begin(), str.end());

  mainwin = CreateWindowEx(
    0,                                         // Optional window styles.
    CLASS_NAME,                                // Window class
    ws.c_str(),                                        // Window text, L"Fernhilfe"
    WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_BORDER | SS_BITMAP,     // Window style
    CW_USEDEFAULT, CW_USEDEFAULT,              //  Position
    545, 295,                                  // Size
    HWND_DESKTOP,                              // Parent window    
    NULL,                                      // Menu
    hInstance,                                 // Instance handle
    NULL);                                     // Additional application data

  if (mainwin == NULL) {
    // TODO
    return 0;
  }

  ShowWindow(mainwin, nCmdShow);
  UpdateWindow(mainwin);

  MSG msg = { };
  while (GetMessage(&msg, NULL, 0, 0)) {      // Run the message loop.
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }

  return msg.wParam;
}

void RunVncServer(const WCHAR *ip)
{
  STARTUPINFO si;
  PROCESS_INFORMATION pi;

  ZeroMemory(&si, sizeof(si));
  si.cb = sizeof(si);
  ZeroMemory(&pi, sizeof(pi));

  std::string str = config["server.path"];
  std::wstring cmdline = std::wstring(str.begin(), str.end());
  cmdline.append(L" ");
  str = config["server.para"];
  cmdline += std::wstring(str.begin(), str.end());

  std::wstring::size_type pos = 0;
  std::wstring::size_type offset = 0;
  std::wstring pattern = L"[IP]";
  std::wstring replace = std::wstring(ip);

  while ((pos = cmdline.find(pattern, offset)) != std::wstring::npos) {
    cmdline.replace(cmdline.begin() + pos, cmdline.begin() + pos + pattern.size(), replace);
    offset = pos + replace.size();
  }

  std::wcout << "RUN: " << cmdline << std::endl;

  //wchar_t cmdline[MAX_PATH] = L"\\\\nas\\Software\\Portable\\UltraVNC 1.3.2\\winvnc.exe -sc_prompt -sc_exit -connect ";
  //wcscat_s(cmdline, MAX_PATH, ip);
  //wcscat_s(cmdline, MAX_PATH, L":5500 -run");

  BOOL rc = CreateProcess(NULL,   // No module name (use command line)
    const_cast<wchar_t *>(cmdline.c_str()),                      // Command line
    NULL,                         // Process handle not inheritable
    NULL,                         // Thread handle not inheritable
    FALSE,                        // Set handle inheritance to FALSE
    0,                            // No creation flags
    NULL,                         // Use parent's environment block
    NULL,                         // Use parent's starting directory
    &si,                          // Pointer to STARTUPINFO structure
    &pi);                         // Pointer to PROCESS_INFORMATION structure.

  if (!rc) {
    MessageBox(mainwin, L"Konnte VNC-Server nicht starten.", L"Error:", MB_OK);
  }
  else {
    PostQuitMessage(0);
  }
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
  std::string str;
  std::wstring ws;

  switch (uMsg) {
    case WM_COMMAND:
      if(((HWND)lParam) && (HIWORD(wParam) == BN_CLICKED)) {
        int iMID = LOWORD(wParam);
        switch(iMID) {
          case 1:
            str = config["ip1"];
            RunVncServer(std::wstring(str.begin(), str.end()).c_str());    // L"192.168.127.1"
            break;
          case 2:
            str = config["ip2"];
            RunVncServer(std::wstring(str.begin(), str.end()).c_str());    // L"192.168.127.2"
            break;
          case 3:
            str = config["ip3"];
            RunVncServer(std::wstring(str.begin(), str.end()).c_str());    // L"192.168.127.3"
            break;
          case 4:
            str = config["ip4"];
            RunVncServer(std::wstring(str.begin(), str.end()).c_str());    // L"192.168.127.4"
            break;
          default:
            break;
        }
      }
      break;

    case WM_CREATE:
      //Gets the directory of the current project and loads logo.bmp

      GetModuleFileNameW(NULL, filename, MAX_PATH);
      //PathRemoveFileSpec(filename);
      PathCchRemoveFileSpec(filename, MAX_PATH);
      //GetCurrentDirectory(MAX_PATH, filename);
      wcscat_s(filename, MAX_PATH, L"\\logo.bmp");

      logoBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL),
                                      filename,
                                      IMAGE_BITMAP,
                                      196, //LR_DEFAULTSIZE,
                                      181, //LR_DEFAULTSIZE,
                                      LR_LOADFROMFILE);
      if (!logoBitmap) {
        ErrorExit(TEXT("Error loading bitmap."));
      }

      str = config["info.text"];
      ws  = std::wstring(str.begin(), str.end());
      txt = CreateWindowW(L"Static", ws.c_str(), WS_VISIBLE | WS_CHILD | SS_LEFT, 20, 40, 250, 30, hwnd, NULL, NULL, NULL);
      str = config["name1"];
      ws  = std::wstring(str.begin(), str.end());
      b1 = CreateWindowW(L"Button", ws.c_str(), WS_VISIBLE | WS_CHILD | SS_LEFT, 20, 80, 250, 30, hwnd, (HMENU)1, NULL, NULL);
      if (config["button1.enabled"] != "true") {
        EnableWindow(b1, FALSE); UpdateWindow(b1);
      }
      str = config["name2"];
      ws  = std::wstring(str.begin(), str.end());
      b2 = CreateWindowW(L"Button", ws.c_str(), WS_VISIBLE | WS_CHILD, 20, 120, 250, 30, hwnd, (HMENU)2, NULL, NULL);
      if (config["button2.enabled"] != "true") {
        EnableWindow(b2, FALSE); UpdateWindow(b2);
      }
      str = config["name3"];
      ws  = std::wstring(str.begin(), str.end());
      b3 = CreateWindowW(L"Button", ws.c_str(), WS_VISIBLE | WS_CHILD, 20, 160, 250, 30, hwnd, (HMENU)3, NULL, NULL);
      if (config["button3.enabled"] != "true") {
        EnableWindow(b3, FALSE); UpdateWindow(b3);
      }

      str = config["name4"];
      ws  = std::wstring(str.begin(), str.end());
      b4 = CreateWindowW(L"Button", ws.c_str(), WS_VISIBLE | WS_CHILD, 20, 200, 250, 30, hwnd, (HMENU)4, NULL, NULL);
      if (config["button4.enabled"] != "true") {
        EnableWindow(b4, FALSE); UpdateWindow(b4);
      }
      //CreateWindowEx(WS_EX_CLIENTEDGE,L"EDIT",L"",WS_CHILD|WS_VISIBLE | ES_PASSWORD,
      //               10,50,125,25,hwnd,(HMENU)textbox_id,NULL,NULL);
      break;

  case WM_CTLCOLORSTATIC:
    {
    if (txt == (HWND)lParam)          //OR use the ctrl ID
    //DWORD CtrlID = GetDlgCtrlID((HWND)lParam); //Window Control ID
    //if (CtrlID == IDC_STATIC1) //If desired control
    {
      HDC hdcStatic = (HDC)wParam;
      SetBkMode(hdcStatic, TRANSPARENT);
      //SetTextColor(hdcStatic, RGB(0,0,0));
      //SetBkColor(hdcStatic, RGB(230,230,230));
      return (INT_PTR)hBrush;
    }
    }

    case WM_DESTROY:
      if (logoBitmap) {
        DeleteObject(logoBitmap); logoBitmap = nullptr;
      }
      PostQuitMessage(0);
      return 0;

    case WM_PAINT:
      {
        HDC dc;
        HDC memdc;
        HGDIOBJ old;
        PAINTSTRUCT ps;
        BITMAP bm;

        dc = BeginPaint(hwnd, &ps);
        FillRect(dc, &ps.rcPaint, (HBRUSH)(COLOR_WINDOW+1));
        memdc = CreateCompatibleDC(dc);
        old = SelectObject(memdc, logoBitmap); 
        GetObject(logoBitmap, sizeof(bm), &bm);
        BitBlt(dc, 300, 230-181, bm.bmWidth, bm.bmHeight, memdc, 0, 0, SRCCOPY);
        SelectObject(memdc, old);
        DeleteDC(memdc);
        EndPaint(hwnd, &ps);
      }
      break;
  }
  return DefWindowProc(hwnd, uMsg, wParam, lParam);
}
